
# Green Power Monitor Test

One Paragraph of project description goes here



In this file I explain how to execute and all the dependencies needed to run this 
proyect.


This are the dependencies you need ot install on your computer to run the proyect:

```
Java 
Maven
```

The versions used for implement and run this proyect had been:
```
Java openjdk version "11.0.11" 2021-04-20
Apache Maven 3.6.0
```

The next commands use Maven to build the download the libraries needed, build the proyect and execute the tests.

To execute the test execute the next command:

```
mvn verify -Dit.test=eu.greenpower.greenpower.runners.CucumberRunnerIT
```

To execute the test with Chrome maximized execute this command:


```
mvn verify -Dit.test=eu.greenpower.greenpower.runners.CucumberRunnerIT -DSHOW_ERRORS_STACKTRACE -DSELENIUM_ARGUMENTS=--start-maximized
```

To execute the test showing the error stack trace execute this commnad:

```
mvn verify -Dit.test=eu.greenpower.greenpower.runners.CucumberRunnerIT -DSHOW_ERRORS_STACKTRACE -DSELENIUM_ARGUMENTS=--start-maximized
```

Finally, to execute the test showing the call stack from Gherkin steps execute this command

```
 mvn verify -Dit.test=eu.greenpower.greenpower.runners.CucumberRunnerIT -DSHOW_ERRORS_STACKTRACE -DSELENIUM_ARGUMENTS=--start-maximized -DSHOW_STACK_INFO
```

RELEASE NOTES
* 0.0.1
    * First version

* **Jose Manuel Pau** - *Initial work* 
  
This projects depends on the GingerSpec framework (QA testing library), where common logic and steps are implemented. 
  
Check more information about the GingerSpec framework [here](https://github.com/veepee-oss/gingerspec/wiki)  