package eu.greenpower.greenpower.glue;

import com.privalia.qa.specs.*;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Glue code of your applications. Use this class to create your own
 * step definitions or cucumber hooks. For more info check:
 * https://cucumber.io/docs/cucumber/step-definitions/
 * https://cucumber.io/docs/cucumber/api/#hooks
 *
 * For more info on how to create your own steps, you can check:
 * https://github.com/veepee-oss/gingerspec/wiki/Creating-your-own-steps
 */
public class CustomStepsDefinition extends BaseGSpec {

    SeleniumGSpec seleniumGSpec;
    RestSpec restSpec;

    /**
     * Example of how to inherit the needed objects from GingerSpec.
     * @param spec the CommonGSpec class
     */
    public CustomStepsDefinition(CommonG spec) {

        this.commonspec = spec;

        /* Access all functions for working with selenium */
        seleniumGSpec = new SeleniumGSpec(this.commonspec);

        /* Access all functions for working with REST services */
        restSpec = new RestSpec(this.commonspec);
    }

    @Before("@disablecookies")
    public void createCustomDriver() throws Throwable {
        /**
         * configuring the driver to disable cookies and set the driver to Gingerspec
         *
         */
        this.seleniumGSpec.getCommonSpec().getDriver().manage().deleteAllCookies();
//        driver.manage().deleteAllCookies();
//        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
//        ChromeOptions options = new ChromeOptions();
//        Map<String, Object> prefs = new HashMap<String, Object>();
//        prefs.put("profile.default_content_settings.cookies", 2);
//        options.setExperimentalOption("prefs", prefs);
//        options.addArguments("start-maximized");
//        ChromeDriver driver = new ChromeDriver(options);
//        this.seleniumGSpec.getCommonSpec().setDriver(driver);
    }

    @When("^I accept the cookies from the browser$")
    public void accept_cookies(){
      this.seleniumGSpec.seleniumClickByLocator("id","onetrust-accept-btn-handler",0);
    }

    @When("^I click on the search icon button$")
    public void click_search_icon_section(){
        this.seleniumGSpec.seleniumClickByLocator("xpath","//*[@id=\"dnvgl\"]/header/section/button[2]",0);
    }

    @And("I type the word {string} in the search box")
    public void type_word_in_search_box(String word) {
        this.seleniumGSpec.seleniumTypeByLocator(word,"id","search-input",null);
    }

    @And("I click on the search button")
    public void click_search_button_to_search() {
        this.seleniumGSpec.seleniumClickByLocator("xpath","//*[@id=\"app-search\"]/section/label[2]",0);
    }

    @And("I click in one search result")
    public void click_link_searched_result() {
        this.seleniumGSpec.seleniumClickByLocator("xpath","//*[@id=\"app-search\"]/main/section[2]/article[1]/a/h2",0);
    }

    @Then("the word {string} is on the search result")
    public void check_word_is_in_search_result(String word) {
        this.close_cookies_popup();
        String pageSource = this.seleniumGSpec.getCommonSpec().getDriver().getPageSource();
        Assert.assertEquals(pageSource.contains(word),true);
    }

    @Then("I click in all the results displayed and the word {string} is on each result")
    public void click_in_results_and_check_word_in_article(String word) {
        String locatortoClick = "xpath";
        String elementtoClick = "";
        for(int i=1;i<=5;i++) {
            elementtoClick = "//*[@id='app-search']/main/section[2]/article["+i+"]/a/h2";
            try {
                this.seleniumGSpec.waitWebElementWithPooling(1, 10, 1, locatortoClick, elementtoClick, "present");
            }
            catch(Exception ex){
                System.out.println("We have not found more results to navigate");
            }
            boolean isPresent = this.seleniumGSpec.getCommonSpec().getDriver().findElements(By.xpath(elementtoClick)).size() > 0;
            if (isPresent) {
                this.seleniumGSpec.seleniumClickByLocator(locatortoClick, elementtoClick, 0);
                this.close_cookies_popup();
                this.check_word_in_result(word);
                this.seleniumGSpec.getCommonSpec().getDriver().navigate().back();
            }
            else{
                break;
            }
        }
    }

    public void check_word_in_result(String word) {
        this.seleniumGSpec.assertSeleniumNElementExists("at least",1,"css","div.article-content__body");
        this.seleniumGSpec.assertSeleniumTextOnElementPresent(0,word);
    }

    public void close_cookies_popup() {
        String elementtoClick = "//*[@id=\"dnvgl\"]/aside/a";
        boolean isPresent = this.seleniumGSpec.getCommonSpec().getDriver().findElements(By.xpath(elementtoClick)).size() > 0;
        if (isPresent) {
            this.seleniumGSpec.seleniumClickByLocator("xpath", elementtoClick, 0);
        }
    }

    @Then("the search has not returned any result")
    public void theSearchHasNotReturnedAnyResult() {
        String elementtoClick = "//*[@id='app-search']/main/section[2]/article[2]/a/h2";
        boolean isPresent = this.seleniumGSpec.getCommonSpec().getDriver().findElements(By.xpath(elementtoClick)).size() > 0;
        Assert.assertEquals(isPresent,false);
    }


}