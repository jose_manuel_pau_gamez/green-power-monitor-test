@web
Feature: Testing search functionality from DNV-GL corporative website

  This feature provides an overview of the posible scenarios you can found when you login a user from
  the mobile App.

  Basically, exist four escenarios.

  1) Searching a word with existing result

  2) Searching a word without existing results

  3) Searching a word with existing results and filter by one filter

  //*[@id="dnvgl"]/header/section/button[2]

  //*[@id="app-search"]/main/section[2]/article[5]/a/h2

  4) Searching a word with existing results and filter by more than one filter

  I have implemented the two first scenarios. I have to say in the first scenario I only check the first search result because for checking all the
  results I have to make a search another time. I have to add the word given in the first scenario is case sensitive.

  You can run this feature directly in Intellij IDEA, for more info check:
  https://github.com/veepee-oss/gingerspec/wiki/Setting-up-your-IDE#running-cucumber-test
  Or by running the following command in the terminal: mvn verify -Dcucumber.filter.tags="@web"

  Scenario Outline: Searching a word with existing result
    Given I go to 'https://www.dnv.com/'
    When I accept the cookies from the browser
    And I click on the search icon button
    And I type the word '<word>' in the search box
    And I click on the search button
    And I click in one search result
    Then the word '<word>' is on the search result
  Examples:
    | word |
    | métricas |
    | Marina |

  Scenario Outline: Searching a word without existing results
    Given I go to 'https://www.dnv.com/'
    When I accept the cookies from the browser
    And I click on the search icon button
    And I type the word '<word>' in the search box
    And I click on the search button
    Then the search has not returned any result

    Examples:
      | word |
      | zzz |
      | yyy |